#%% init
#Variable reset
%reset -f

#Import relevant libaries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import statistics
import os
import dill 
import csv 

#import plot functions
#import plot_functions

def parse_if_number(s):
    try: return float(s)
    except: return True if s=="true" else False if s=="false" else s if s else None

def parse_ndarray(s):
    return np.fromstring(s, sep=' ') if s else None

# Find all csv folders
csv_path = "/home/ubuntu/p10/python/csv_files"
subfolders = [f.path for f in os.scandir(csv_path) if f.is_dir()]
subfolders.remove("/home/ubuntu/p10/python/csv_files/.ipynb_checkpoints")
subfolders.append("/home/ubuntu/p10/python/csv_files")

#Find all pkl files in csv folders
data_files = []
data_dirs = []
for folder in subfolders:
    print("pkl files found in "+folder+": ")
    for file in os.listdir(folder):
        filename = os.fsdecode(file)
        if filename.endswith(".pkl"): 
            data_files.append(filename)
            data_dirs.append(folder+"/"+filename)
            print(filename)
        else:
            continue

# Find all txt files that indicate a figure config
figures_path = "/home/ubuntu/p10/python/figures"
txt_files = []
txt_dirs = []
print("\ntxt files found in "+figures_path+": ")
for file in os.listdir(figures_path):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"): 
        txt_files.append(filename)
        txt_dirs.append(folder+"/"+filename)
        print(filename)
    else:
        continue

#Find all exsisting figure folders
figure_folders = [f.path for f in os.scandir(figures_path) if f.is_dir()]
figure_folders.remove("/home/ubuntu/p10/python/figures/.ipynb_checkpoints")

#See if the figures already exsist
new_folders = []
print("\nNew folders made: ")
for figure in txt_files:
    if (figures_path+"/"+figure.replace(".txt","")) not in figure_folders:
        os.mkdir(figures_path+"/"+figure.replace(".txt",""))
        new_folders.append(figures_path+"/"+figure.replace(".txt",""))
        print(figures_path+"/"+figure.replace(".txt",""))
    else:
        continue

#flags(set to 1/True for plot, or 0/False for no plot)
plr_flag = False
delay_flag = False
overhead_flag = False
individual_overhead_flag = False
util_flag = False
util_plr_flag = False
#figure vars
color_list = ["cornflowerblue","tomato","orange","mediumseagreen","violet"] # color to be used for plotting
marker_list = ["o","*","x","^","d"] # marker style to be used for plotting
line_list = ["solid","dotted","dashed","dashdot","solid"] # line style to be used for plotting

for figure in new_folders:
    # reading csv file 
    fig_name = figure+figure.replace(figures_path,"")+"_" #dir and base name for figures
    with open(figure+".txt", 'r') as csvfile: 
        # creating a csv reader object 
        csvreader = csv.reader(csvfile,delimiter=',') 
        #read content
        rows = []
        input_files = [] 
        names_list = [] 
        for row in csvreader: 
            if row[0][0][0] != "#":
                if row[0][0][0] == "!":
                    row[0] = row[0].replace("!","")
                    for flags in row:
                        flags = flags.strip()
                        if flags == "plr_flag":
                            plr_flag = True
                        if flags == "delay_flag":
                            delay_flag = True
                        if flags == "overhead_flag":
                            overhead_flag = True
                        if flags == "individual_overhead_flag":
                            individual_overhead_flag = True
                        if flags == "util_flag":
                            util_flag = True
                        if flags == "util_plr_flag":
                            util_plr_flag = True
                else:
                    rows.append(row)
        for i in range(len(rows)):
            input_files.append(data_dirs[data_files.index(rows[i][0])])
            names_list.append(rows[i][1])
            print(input_files[i])
        
    #figure declaration need to plot multiple times on one figure
    if plr_flag:
        fig_plr, ax_plr = plt.subplots(figsize=(8, 4))
        #print("Packet loss ratio flag set")
    if delay_flag:
        fig_delay, ax_delay = plt.subplots(figsize=(8, 4))
        max_delay = 0 #for axis range
        #print("Delay flag set")
    if overhead_flag:
        fig_over, ax_over = plt.subplots(figsize=(8, 4))
        max_overhead = 0 #for axis range
        #print("Overhead flag set")
    if individual_overhead_flag:
        fig_UDP, ax_UDP = plt.subplots(figsize=(8, 4))
        fig_ELP, ax_ELP = plt.subplots(figsize=(8, 4))
        fig_probe, ax_probe = plt.subplots(figsize=(8, 4))
        fig_OGM, ax_OGM = plt.subplots(figsize=(8, 4))
        max_UDP_overhead = 0 #for axis range
        max_ELP_overhead = 0 #for axis range
        max_probe_overhead = 0 #for axis range
        max_OGM_overhead = 0 #for axis range
        #print("Individual overhead flag set")
    if util_flag:
        fig_util, ax_util = plt.subplots(figsize=(8, 4))
        max_util = 0 #for axis range
    if util_plr_flag:
        fig_util_plr, ax_util_plr = plt.subplots(figsize=(8, 4))

    #Go through every file
    iterator2 = 0
    for current_file in input_files:
        dill.load_session(current_file)
        names = names_list[iterator2]
        iterator2 += 1
    #################################################################
        if plr_flag:
            #extract values from each entry
            plr = [0]*len(sorted_name)
            for i in range(len(sorted_name)):
                tmp_send = send_UDP_count[(send_UDP_count.run==sorted_name[i])]
                tmp_rcvd = rcvd_UDP_count[(rcvd_UDP_count.run==sorted_name[i])]
                cum_send = (sum(tmp_send.value))
                cum_rcvd = (sum(tmp_rcvd.value))
                plr[i] = 1-(cum_rcvd/cum_send)

            #declare vars for memory allocation(faster execution)
            mean_plr = [0]*N
            lower = [0]*N
            upper = [0]*N
            plot_n = [0]*N

            #calc means and confidence intervals if multiple realizations otherwise just use values
            if realizations > 1:
                for i in range(0,N*realizations,realizations):
                    #print(i) #debug
                    mean_plr[int(i/realizations)] = statistics.mean(plr[i:i+realizations])
                    #print(plr[i:i+realizations]) #debug
                    var = statistics.stdev(plr[i:i+realizations])
                    #print(var) #debug
                    upper[int(i/realizations)] = mean_plr[int(i/realizations)] + 1.96 * (var/np.sqrt(realizations))
                    lower[int(i/realizations)] = mean_plr[int(i/realizations)] - 1.96 * (var/np.sqrt(realizations))
                    plot_n[int(i/realizations)] = sorted_N[i]
            else:
                for i in range(N):
                    mean_plr[i] = plr[i]
                    plot_n[i] = sorted_N[i]

            #plot everything
            ax_plr.plot(plot_n, mean_plr, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], color=color_list[iterator2-1], label=names+" mean PLR")
            if realizations > 1:
                ax_plr.fill_between(plot_n, lower, upper, color=color_list[iterator2-1], alpha=.1,label=names+" 95% confidence interval")
            #figure formatting, only run on last iteration
            if current_file == input_files[-1]:
                ax_plr.axis([min(para_N), max(para_N), 0, 1])
                ax_plr.grid()
                box = ax_plr.get_position()
                ax_plr.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
                #legend and axis text
                ax_plr.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
                ax_plr.set_xlabel("Number of nodes")
                ax_plr.set_ylabel("Packet loss ratio")
                #finish figure and save it
                fig_plr.show()
                fig_plr.savefig(fig_name+'_plr.pdf')

    ####################################################################
        if delay_flag:
            #extract values from each entry
            lifetime = [0]*len(sorted_name)
            for i in range(len(sorted_name)):
                tmp_vec = rcvdPkLifetime_vec[(rcvdPkLifetime_vec.run==sorted_name[i])]  # filter rows
                if not tmp_vec.empty:
                    tmp_vec_value = tmp_vec.iloc[0,8]
                    lifetime[i] = np.mean(tmp_vec_value)

            #declare vars for memory allocation(faster execution)
            mean_lifetime = [0]*N
            lower = [0]*N
            upper = [0]*N
            plot_n = [0]*N
                    
            #calc means and confidence intervals if multiple realizations otherwise just use values
            if realizations > 1:
                for i in range(0,N*realizations,realizations):
                    mean_lifetime[int(i/realizations)] = statistics.mean(lifetime[i:i+realizations])
                    var = statistics.stdev(lifetime[i:i+realizations])
                    upper[int(i/realizations)] = mean_lifetime[int(i/realizations)] + 1.96 * (var/np.sqrt(realizations))
                    lower[int(i/realizations)] = mean_lifetime[int(i/realizations)] - 1.96 * (var/np.sqrt(realizations))
                    plot_n[int(i/realizations)] = sorted_N[i]
            else:
                for i in range(N):
                    mean_lifetime[i] = lifetime[i]
                    plot_n[i] = sorted_N[i]

            #plot everything  
            ax_delay.plot(plot_n, mean_lifetime, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], color=color_list[iterator2-1],label=names+" mean E2E delay")
            if realizations > 1:    
                ax_delay.fill_between(plot_n, lower, upper, color=color_list[iterator2-1], alpha=.1,label=names+" 95% confidence interval")
            #figure formatting, only run on last iteration
            max_delay = max(max_delay,max(mean_lifetime))
            if current_file == input_files[-1]:
                ax_delay.axis([min(para_N), max(para_N), 0, max_delay+0.02])
                ax_delay.grid()
                #ax_delay.legend(loc="best")
                box = ax_delay.get_position()
                ax_delay.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
                #legend and axis text
                ax_delay.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
                ax_delay.set_xlabel("Number of nodes")
                ax_delay.set_ylabel("Seconds")
                #finish figure and save it
                fig_delay.show()
                fig_delay.savefig(fig_name+'_delay.pdf')

    #############################################################
        if overhead_flag or individual_overhead_flag:
            #extract values from each entry
            UDP_overhead = [0]*len(sorted_name)
            ELP_overhead = [0]*len(sorted_name)
            ELPProbe_overhead = [0]*len(sorted_name)
            OGM_overhead = [0]*len(sorted_name)
            for i in range(len(sorted_name)):
                tmp_forward = forward_UDP_sum[(forward_UDP_sum.run==sorted_name[i])]  # filter rows
                UDP_overhead[i] = ((sum(tmp_forward.value))/sim_time)/sorted_N[i]

                tmp_ELP = send_ELP_sum[(send_ELP_sum.run==sorted_name[i])]  # filter rows
                ELP_overhead[i] = (sum(tmp_ELP.value)/sim_time)/sorted_N[i]

                tmp_ELPProbe = send_ELPProbes_sum[(send_ELPProbes_sum.run==sorted_name[i])]  # filter rows
                ELPProbe_overhead[i] = (sum(tmp_ELPProbe.value)/sim_time)/sorted_N[i]

                tmp_OGM = send_OGM_sum[(send_OGM_sum.run==sorted_name[i])]  # filter rows
                OGM_overhead[i] = (sum(tmp_OGM.value)/sim_time)/sorted_N[i]

            #declare vars for memory allocation(faster execution)
            mean_UDP = [0]*N
            lower_UDP = [0]*N
            upper_UDP = [0]*N

            mean_ELP = [0]*N
            lower_ELP = [0]*N
            upper_ELP = [0]*N

            mean_probe = [0]*N
            lower_probe = [0]*N
            upper_probe = [0]*N

            mean_OGM = [0]*N
            lower_OGM = [0]*N
            upper_OGM = [0]*N

            plot_n = [0]*N

            #calc means and confidence intervals if multiple realizations otherwise just use values
            if realizations > 1:
                for i in range(0,N*realizations,realizations):
                    mean_UDP[int(i/realizations)] = statistics.mean(UDP_overhead[i:i+realizations])
                    var = statistics.stdev(UDP_overhead[i:i+realizations])
                    upper_UDP[int(i/realizations)] = mean_UDP[int(i/realizations)] + 1.96 * (var/np.sqrt(realizations))
                    lower_UDP[int(i/realizations)] = mean_UDP[int(i/realizations)] - 1.96 * (var/np.sqrt(realizations))

                    mean_ELP[int(i/realizations)] = statistics.mean(ELP_overhead[i:i+realizations])
                    var = statistics.stdev(ELP_overhead[i:i+realizations])
                    upper_ELP[int(i/realizations)] = mean_ELP[int(i/realizations)] + 1.96 * (var/np.sqrt(realizations))
                    lower_ELP[int(i/realizations)] = mean_ELP[int(i/realizations)] - 1.96 * (var/np.sqrt(realizations))

                    mean_probe[int(i/realizations)] = statistics.mean(ELPProbe_overhead[i:i+realizations])
                    var = statistics.stdev(ELPProbe_overhead[i:i+realizations])
                    upper_probe[int(i/realizations)] = mean_probe[int(i/realizations)] + 1.96 * (var/np.sqrt(realizations))
                    lower_probe[int(i/realizations)] = mean_probe[int(i/realizations)] - 1.96 * (var/np.sqrt(realizations))

                    mean_OGM[int(i/realizations)] = statistics.mean(OGM_overhead[i:i+realizations])
                    var = statistics.stdev(OGM_overhead[i:i+realizations])
                    upper_OGM[int(i/realizations)] = mean_OGM[int(i/realizations)] + 1.96 * (var/np.sqrt(realizations))
                    lower_OGM[int(i/realizations)] = mean_OGM[int(i/realizations)] - 1.96 * (var/np.sqrt(realizations))

                    plot_n[int(i/realizations)] = sorted_N[i]
            else:
                for i in range(N):
                    mean_UDP[i] = UDP_overhead[i]
                    mean_ELP[i] = ELP_overhead[i]
                    mean_probe[i] = ELPProbe_overhead[i]
                    mean_OGM[i] = OGM_overhead[i]
                    plot_n[i] = sorted_N[i]

            if overhead_flag:  
                #plot everything  
                ax_over.plot(plot_n, mean_UDP, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], label=names+" UDP overhead")
                ax_over.plot(plot_n, mean_ELP, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], label=names+" ELP overhead")
                ax_over.plot(plot_n, mean_probe, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], label=names+" ELP probe overhead")
                ax_over.plot(plot_n, mean_OGM, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], label=names+" OGM overhead")
                if realizations > 1:    
                    ax_over.fill_between(plot_n, lower_UDP, upper_UDP, color='b', alpha=.1,label=names+" UDP 95% confidence interval")
                    ax_over.fill_between(plot_n, lower_ELP, upper_ELP, color='orange', alpha=.1,label=names+" ELP 95% confidence interval")
                    ax_over.fill_between(plot_n, lower_probe, upper_probe, color='g', alpha=.1,label=names+" probe 95% confidence interval")
                    ax_over.fill_between(plot_n, lower_OGM, upper_OGM, color='r', alpha=.1,label=names+" OGM 95% confidence interval")
                #figure formatting, only run on last iteration
                max_overhead = max(max_overhead,max(mean_UDP))
                if current_file == input_files[-1]:
                    ax_over.axis([min(para_N), max(para_N), 0, max_overhead+2000])
                    ax_over.grid()
                    #ax_over.legend(loc="best")
                    box = ax_over.get_position()
                    ax_over.set_position([box.x0, box.y0 + box.height * 0.3, box.width, box.height * 0.7])
                    #legend and axis text
                    ax_over.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
                    ax_over.set_xlabel("Number of nodes")
                    ax_over.set_ylabel("Mean bytes per second per node")
                    #finish figure and save it
                    fig_over.show()
                    #fig_over.savefig('Probe_overhead.pdf')
                    fig_over.savefig(fig_name+'_overhead.pdf')

            if individual_overhead_flag:  
                #plot everything    
                ax_UDP.plot(plot_n, mean_UDP, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], color=color_list[iterator2-1], label=names+" UDP overhead")
                if realizations > 1:    
                    ax_UDP.fill_between(plot_n, lower_UDP, upper_UDP, color=color_list[iterator2-1], alpha=.1,label=names+" UDP 95% confidence interval")
                #figure formatting, only run on last iteration
                max_UDP_overhead = max(max_UDP_overhead,max(mean_UDP))
                if current_file == input_files[-1]:
                    ax_UDP.axis([min(para_N), max(para_N), 0, max_UDP_overhead+2000])
                    ax_UDP.grid()
                    box = ax_UDP.get_position()
                    ax_UDP.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
                    ax_UDP.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
                    #legend and axis text
                    ax_UDP.set_xlabel("Number of nodes")
                    ax_UDP.set_ylabel("Mean bytes per second per node")
                    #finish figure and save it
                    fig_UDP.show()
                    fig_UDP.savefig(fig_name+'_UDP_overhead.pdf')

                #plot everything   
                ax_ELP.plot(plot_n, mean_ELP, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], color=color_list[iterator2-1], label=names+" ELP overhead")
                if realizations > 1: 
                    ax_ELP.fill_between(plot_n, lower_ELP, upper_ELP, color=color_list[iterator2-1], alpha=.1,label=names+" UDP 95% confidence interval")
                #figure formatting, only run on last iteration
                max_ELP_overhead = max(max_ELP_overhead,max(mean_ELP))
                if current_file == input_files[-1]:
                    ax_ELP.axis([min(para_N), max(para_N), 0, max_ELP_overhead+100])
                    ax_ELP.grid()
                    box = ax_ELP.get_position()
                    ax_ELP.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
                    ax_ELP.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
                    #legend and axis text
                    ax_ELP.set_xlabel("Number of nodes")
                    ax_ELP.set_ylabel("Mean bytes per second per node")
                    #finish figure and save it
                    fig_ELP.show()
                    fig_ELP.savefig(fig_name+'_ELP_overhead.pdf')

                #plot everything 
                ax_probe.plot(plot_n, mean_probe, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], color=color_list[iterator2-1], label=names+" ELP probe overhead")
                if realizations > 1: 
                    ax_probe.fill_between(plot_n, lower_probe, upper_probe, color=color_list[iterator2-1], alpha=.1,label=names+" UDP 95% confidence interval")
                #figure formatting, only run on last iteration
                max_probe_overhead = max(max_probe_overhead,max(mean_probe))
                if current_file == input_files[-1]:
                    ax_probe.axis([min(para_N), max(para_N), 0, max_probe_overhead+500])
                    ax_probe.grid()
                    box = ax_probe.get_position()
                    ax_probe.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
                    ax_probe.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
                    #legend and axis text
                    ax_probe.set_xlabel("Number of nodes")
                    ax_probe.set_ylabel("Mean bytes per second per node")
                    #finish figure and save it
                    fig_probe.show()
                    fig_probe.savefig(fig_name+'_probe_overhead.pdf')

                #plot everything 
                ax_OGM.plot(plot_n, mean_OGM, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], color=color_list[iterator2-1], label=names+" OGM overhead")
                if realizations > 1: 
                    ax_OGM.fill_between(plot_n, lower_OGM, upper_OGM, color=color_list[iterator2-1], alpha=.1,label=names+" UDP 95% confidence interval")
                #figure formatting, only run on last iteration
                max_OGM_overhead = max(max_OGM_overhead,max(mean_OGM))
                if current_file == input_files[-1]:
                    ax_OGM.axis([min(para_N), max(para_N), 0, max_OGM_overhead+100])
                    ax_OGM.grid()
                    box = ax_OGM.get_position()
                    ax_OGM.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
                    ax_OGM.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
                    #legend and axis text
                    ax_OGM.set_xlabel("Number of nodes")
                    ax_OGM.set_ylabel("Mean bytes per second per node")
                    #finish figure and save it
                    fig_OGM.show()
                    fig_OGM.savefig(fig_name+'_OGM_overhead.pdf')

        if util_flag:
            #extract values from each entry
            util = [0]*len(sorted_sep_name)
            for i in range(len(sorted_sep_name)):
                tmp_vec = util_sent_bits[(util_sent_bits.run==sorted_sep_name[i])]
                if not tmp_vec.empty:
                    tmp_vec_value = tmp_vec.value
                    util[i] = np.mean(tmp_vec_value)

            #declare vars for memory allocation(faster execution)
            mean_util = [0]*N
            lower = [0]*N
            upper = [0]*N
            plot_n = [0]*N

            #calc means and confidence intervals if multiple realizations otherwise just use values
            if realizations > 1:
                for i in range(0,N*realizations,realizations):
                    mean_util[int(i/realizations)] = statistics.mean(util[i:i+realizations])
                    var = statistics.stdev(util[i:i+realizations])
                    upper[int(i/realizations)] = mean_util[int(i/realizations)] + 1.96 * (var/np.sqrt(realizations))
                    lower[int(i/realizations)] = mean_util[int(i/realizations)] - 1.96 * (var/np.sqrt(realizations))
                    plot_n[int(i/realizations)] = sorted_sep[i]
            else:
                for i in range(N):
                    mean_util[i] = util[i]
                    plot_n[i] = sorted_sep[i]/max(sorted_sep)

            #plot everything  
            ax_util.plot(plot_n, mean_util, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], color=color_list[iterator2-1],label=names+" mean utilization")
            if realizations > 1:    
                ax_util.fill_between(plot_n, lower, upper, color=color_list[iterator2-1], alpha=.1,label=names+" 95% confidence interval")
            #figure formatting, only run on last iteration
            max_util = max(max_util,max(mean_util))
            if current_file == input_files[-1]:
                ax_util.axis([0, 1, 0, max_util])
                ax_util.grid()
                #ax_delay.legend(loc="best")
                box = ax_util.get_position()
                ax_util.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
                #legend and axis text
                ax_util.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
                ax_util.set_xlabel("Ratio between communication rangeand interference range")
                ax_util.set_ylabel("Mean used airtime")
                #finish figure and save it
                fig_util.show()
                fig_util.savefig(fig_name+'_util.pdf')

        if util_plr_flag:
            #extract values from each entry
            plr = [0]*len(sorted_name)
            for i in range(len(sorted_name)):
                tmp_send = send_UDP_count[(send_UDP_count.run==sorted_name[i])]
                tmp_rcvd = rcvd_UDP_count[(rcvd_UDP_count.run==sorted_name[i])]
                cum_send = (sum(tmp_send.value))
                cum_rcvd = (sum(tmp_rcvd.value))
                plr[i] = 1-(cum_rcvd/cum_send)

            #declare vars for memory allocation(faster execution)
            mean_plr = [0]*N
            lower = [0]*N
            upper = [0]*N
            plot_n = [0]*N

            #calc means and confidence intervals if multiple realizations otherwise just use values
            if realizations > 1:
                for i in range(0,N*realizations,realizations):
                    #print(i) #debug
                    mean_plr[int(i/realizations)] = statistics.mean(plr[i:i+realizations])
                    #print(plr[i:i+realizations]) #debug
                    var = statistics.stdev(plr[i:i+realizations])
                    #print(var) #debug
                    upper[int(i/realizations)] = mean_plr[int(i/realizations)] + 1.96 * (var/np.sqrt(realizations))
                    lower[int(i/realizations)] = mean_plr[int(i/realizations)] - 1.96 * (var/np.sqrt(realizations))
                    plot_n[int(i/realizations)] = sorted_N[i]
            else:
                for i in range(N):
                    mean_plr[i] = plr[i]
                    plot_n[i] = sorted_sep[i]/max(sorted_sep)

            #plot everything  
            ax_util_plr.plot(plot_n, mean_plr, linestyle=line_list[iterator2-1], marker=marker_list[iterator2-1], color=color_list[iterator2-1],label=names+" mean utilization")
            if realizations > 1:    
                ax_util_plr.fill_between(plot_n, lower, upper, color=color_list[iterator2-1], alpha=.1,label=names+" 95% confidence interval")
            #figure formatting, only run on last iteration
            if current_file == input_files[-1]:
                ax_util_plr.axis([0, 1, 0, 1])
                ax_util_plr.grid()
                #ax_delay.legend(loc="best")
                box = ax_util_plr.get_position()
                ax_util_plr.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
                #legend and axis text
                ax_util_plr.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
                ax_util_plr.set_xlabel("Distance between nodes")
                ax_util_plr.set_ylabel("Packet loss ratio")
                #finish figure and save it
                fig_util_plr.show()
                fig_util_plr.savefig(fig_name+'_util_plr.pdf')


    print("\n")
    # %%

# %%
