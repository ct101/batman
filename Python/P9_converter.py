#%% init
#Variable reset
%reset -f

#Import relevant libaries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import statistics
import os
import dill 

def parse_if_number(s):
    try: return float(s)
    except: return True if s=="true" else False if s=="false" else s if s else None

def parse_ndarray(s):
    return np.fromstring(s, sep=' ') if s else None

#%% Load dataset
#list of files to be processed and plotted together
csv_path = "/home/ubuntu/p10/python/csv_files"
subfolders = [f.path for f in os.scandir(csv_path) if f.is_dir()]
subfolders.append("/home/ubuntu/p10/python/csv_files")
subfolders.remove("/home/ubuntu/p10/python/csv_files/.ipynb_checkpoints")

csv_files = []
csv_names = []
pkl_files = []
for folder in subfolders:
    print("csv files found in "+folder+": ")
    for file in os.listdir(folder):
        filename = os.fsdecode(file)
        if filename.endswith(".csv"): 
            csv_files.append(folder+"/"+filename)
            csv_names.append(filename)
            print(filename)
        if filename.endswith(".pkl"): 
            pkl_files.append(filename)
        else:
            continue
#csv_files = ["/home/ubuntu/p10/python/csv_files/tmp3.csv"]
#%%

#Go through every file
iterator1 = 0
for target in csv_files:
    iterator1 += 1
    print("\nCurrent target file: "+target)
    if csv_names[iterator1-1].replace(".csv", ".pkl") in pkl_files:
        print("Skipped, already processed")
        
    else:
        #var declaration
        para_name = []
        para_N = []

        #list to store different statistics from each chunk read
        send_UDP_count = pd.DataFrame()
        rcvd_UDP_count = pd.DataFrame()
        rcvdPkLifetime_vec = pd.DataFrame()
        send_UDP_sum = pd.DataFrame()
        forward_UDP_sum = pd.DataFrame()
        send_ELP_sum = pd.DataFrame()
        send_ELPProbes_sum = pd.DataFrame()
        send_OGM_sum = pd.DataFrame()

        #time vars, can be overwritten here if code is incompatible
        sim_start = 0
        sim_stop = 0

        chunksize = 10 ** 4
        progress = 0
        for chunk in pd.read_csv(target, usecols=['run', 'type', 'module','attrname','attrvalue', 'name', 'value','vectime','vecvalue'],converters = {'attrvalue': parse_if_number,'vectime': parse_ndarray,'vecvalue': parse_ndarray}, chunksize=chunksize):
            progress = progress +1

            #if this fails check line 80-82 or line 133-135
            if sim_start == 0:
                sim_start = chunk[(chunk.attrname=='startTime')]  # filter rows
                if not sim_start.empty:
                    sim_start = sim_start.at[sim_start.index[0],"attrvalue"]
                    sim_start = int(sim_start)
                    #print(sim_start)
                else:
                    sim_start = 0
            if sim_stop == 0:    
                sim_stop = chunk[(chunk.attrname=='stopTime')]  # filter rows
                if not sim_stop.empty:
                    sim_stop = sim_stop.at[sim_stop.index[0],"attrvalue"]
                    sim_stop = int(sim_stop)
                    #print(sim_stop)
                else:
                    sim_stop = 0

            #Find run names for parameter N
            para = chunk[(chunk.attrname=='N')]  # filter rows
            index = para.index
            for i in range(len(para)):
                j = index[i] #way of getting specific entry, keep in mind that i does not follow 0,1,2...
                para_name.append(para.run[j]) # save name
                para_N.append(int(para.attrvalue[j])) # and in same order matching N

            #start reading out stats
            #plr
            send_UDP_count = send_UDP_count.append(chunk[(chunk.name=='sentPk:count') & (chunk.type=='scalar')])  # filter rows
            rcvd_UDP_count = rcvd_UDP_count.append(chunk[(chunk.name=='rcvdPk:count') & (chunk.type=='scalar')])  # filter rows

            #delay
            rcvdPkLifetime_vec = rcvdPkLifetime_vec.append(chunk[(chunk.name=='rcvdPkLifetime:vector')]) 

            #overhead
            send_UDP_sum = send_UDP_sum.append(chunk[(chunk.name=='sendUDP:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
            forward_UDP_sum = forward_UDP_sum.append(chunk[(chunk.name=='ForwardedUnicasts:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
            send_ELP_sum  = send_ELP_sum.append(chunk[(chunk.name=='sendELP:sum(packetBytes)') & (chunk.type=='scalar')])# filter rows
            send_ELPProbes_sum  = send_ELPProbes_sum.append(chunk[(chunk.name=='sendELPProbes:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
            send_OGM_sum  = send_OGM_sum.append(chunk[(chunk.name=='sendOGMv2:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows

        #calculate sim time for stats later
        sim_time = sim_stop - sim_start
        if sim_time == 0:
            sim_time = 3000
            print("Failed to get logging duration from csv file, using deafult legacy value of 3000s")

        #way of sorting runs into order of N lowest to highest(makes plotting easier)
        para_zip = zip(para_N,para_name)
        sorted_para = sorted(para_zip)
        tuples = zip(*sorted_para)
        #sorted lists ready for use
        sorted_N, sorted_name = [ list(tuple) for tuple in  tuples]
        N = len(np.unique(para_N))
        realizations = int(len(para_N)/len(np.unique(para_N)))

        %reset_selective -f chunk

        dill.dump_session(target.replace(".csv", ".pkl")) # Save the session
        print("Saved workspace as: "+target.replace(".csv", ".pkl"))


# %%
