#%% Imports and def's
import matplotlib.pyplot as plt
import matplotlib as mpl
import networkx as nx
import netgraph
import pandas as pd
import numpy as np
import re
from collections import Counter
import copy
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Used for "cleaning" pandas column, i.e. transforming it from a string to a list
def clean_alt_list(list_):
    list_ = list_.replace(' ', '","')
    list_ = ''.join(('["',list_,'"]'))
    return list_

# Count occurences of each item in given list, used on a whole pandas column
def counter_for_list(list_):
    list_ = list(Counter(list_).items())
    return list_

def getNodes(list_):
    return ([list(zip(*list_))[0]])

def getWeight(list_):
    return ([list(zip(*list_))[1]])

# Takes min and max value for data range, and return rgb triplet for specified color map
def color_map_color(value, cmap_name='viridis', vmin=0, vmax=1):
    norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
    cmap = mpl.cm.get_cmap(cmap_name)
    rgb = cmap(norm(value))[:3]
    return rgb

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

#%% Load CSV file
#grid = pd.read_csv("/root/p10/python/csv_files/baseline_OVERWRITE/baseline_low_traffic_OVERWRITE.csv", delimiter = ",")
#grid = pd.read_csv("/root/p10/python/csv_files/baseline_bitrate_estimation_TAPT/baseline_low_traffic_TAPT.csv", delimiter = ",")
#grid = pd.read_csv("/root/p10/python/csv_files/baseline_OVERWRITE/baseline_high_traffic_OVERWRITE.csv", delimiter = ",")
#grid = pd.read_csv("/root/p10/python/csv_files/bitrate_circle/baseline_high_traffic_tapt_bitrate_5.csv", delimiter = ",")
#grid = pd.read_csv("/home/greth/Documents/P10/batman-old/batman/inet/examples/adhoc/batmanadv/results/bitrate_estimation_circle_OVERWRITE_5.csv", delimiter = ",")
#grid = pd.read_csv("/root/p10/python/csv_files/baseline_quick_test/blt_4000.csv", delimiter = ",")

#grid = pd.read_csv("/root/p10/python/csv_files/usage_estimation/usage_estimation_circle_TAPT.csv", delimiter = ",")
#grid = pd.read_csv("/root/p10/python/csv_files/usage_estimation/usage_estimation_circle_OVERWRITE.csv", delimiter = ",")

#grid = pd.read_csv("/root/p10/python/csv_files/baseline_bitrate-usage_TAPT/blt_tapt-usage_200.csv", delimiter = ",")
#grid = pd.read_csv("/root/p10/python/csv_files/baseline_bitrate-usage_TAPT/bht_tapt-usage_200.csv", delimiter = ",")
#grid = pd.read_csv("/root/p10/python/csv_files/blt_small_ogm_delay_OVERWRITE.csv", delimiter = ",")
#grid = pd.read_csv("/root/p10/python/csv_files/blt_manualestimation_200.csv", delimiter = ",")

#grid = pd.read_csv("/root/p10/python/csv_files/loss_esti_circle/loss_esti_circle_wloss_wextra_tapt.csv", delimiter = ",")

#%% 
runs = list(grid.run.unique())
runs.sort(key=natural_keys)
for i in runs:
    print("=======================================================")
    print("Currently working on run: {}".format(i))
    print("Run: {} / {}".format(runs.index(i)+1, len(runs)))

    # rcvd unicast packets from and send unicast packets from
    df_rcvd = grid[ (grid['run'] == i) & ( (grid['type'] == 'vector') &  (grid['name'] == 'rcvdUniPkFrom:vector') ) ].copy()
    df_send = grid[ (grid['run'] == i) & ( (grid['type'] == 'vector') &  (grid['name'] == 'sendUniPkTo:vector') ) ].copy()
    df_cord = grid[ (grid['run'] == i) & ( (grid['type'] == 'vector') & ((grid['name'] == 'coordX:vector') | (grid['name'] == 'coordY:vector')) ) ].copy()

    # Clean the vecvalue field so we actually can work with it, and transform it from string to a list
    df_rcvd['vecvalue'] = df_rcvd['vecvalue'].apply(clean_alt_list)
    df_rcvd['vecvalue'] = df_rcvd['vecvalue'].apply(eval)

    df_send['vecvalue'] = df_send['vecvalue'].apply(clean_alt_list)
    df_send['vecvalue'] = df_send['vecvalue'].apply(eval)

    df_cord['vecvalue'] = df_cord['vecvalue'].apply(clean_alt_list)
    df_cord['vecvalue'] = df_cord['vecvalue'].apply(eval)

    # Count occurrences of each node id for each row and return new collumn with data in it
    vecvalue_counted_rcvd = df_rcvd['vecvalue'].apply(counter_for_list)
    df_rcvd['vecvalue_counted_rcvd'] = vecvalue_counted_rcvd

    vecvalue_counted_send = df_send['vecvalue'].apply(counter_for_list)
    df_send['vecvalue_counted_send'] = vecvalue_counted_send
    
    #regex for getting the id between the [], and add it to the dataframe
    node_ids = df_rcvd['module'].str.extract(r'\[(.*?)\]')    
    df_rcvd['node_ids'] = node_ids
    df_rcvd['node_ids'] = pd.to_numeric(df_rcvd['node_ids'], errors='coerce')
    
    node_ids = df_send['module'].str.extract(r'\[(.*?)\]')    
    df_send['node_ids'] = node_ids
    df_send['node_ids'] = pd.to_numeric(df_send['node_ids'], errors='coerce')

    node_ids = df_cord['module'].str.extract(r'\[(.*?)\]')    
    df_cord['node_ids'] = node_ids
    df_cord['node_ids'] = pd.to_numeric(df_cord['node_ids'], errors='coerce')
    node_pos_info       = (df_cord['node_ids']).tolist()

    # Stuff for rcvd plot
    rcvd_links_dst    = (df_rcvd['node_ids']).tolist()
    rcvd_links_src    = (df_rcvd['vecvalue_counted_rcvd'].apply(getNodes)).tolist()
    rcvd_links_weight = (df_rcvd['vecvalue_counted_rcvd'].apply(getWeight)).tolist()

    # Turns the values into ints instead of strings
    for j in range(len(rcvd_links_src)):
        rcvd_links_src[j][0] = tuple(map(int, rcvd_links_src[j][0]))

    # Turns the values into ints instead of strings
    for j in range(len(rcvd_links_weight)):
        rcvd_links_weight[j][0] = tuple(map(int, rcvd_links_weight[j][0]))

    # rcvd_sdw is an edge list that contains (src, dst, weight) pairs
    rcvd_sdw = list()
    for j in range(len(rcvd_links_dst)):
        for k in range(len(rcvd_links_src[j][0])):
            rcvd_sdw.append( (rcvd_links_src[j][0][k], rcvd_links_dst[j], rcvd_links_weight[j][0][k]) )


    # Stuff for send plot
    send_links_dst    = (df_send['node_ids']).tolist()
    send_links_src    = (df_send['vecvalue_counted_send'].apply(getNodes)).tolist()
    send_links_weight = (df_send['vecvalue_counted_send'].apply(getWeight)).tolist()

    # Turns the values into ints instead of strings
    for j in range(len(send_links_src)):
        send_links_src[j][0] = tuple(map(int, send_links_src[j][0]))

    # Turns the values into ints instead of strings
    for j in range(len(send_links_weight)):
        send_links_weight[j][0] = tuple(map(int, send_links_weight[j][0]))

    # send_sdw is an edge list that contains (src, dst, weight) pairs
    send_sdw = list()
    for j in range(len(send_links_dst)):
        for k in range(len(send_links_src[j][0])):
            send_sdw.append( (send_links_dst[j], send_links_src[j][0][k], send_links_weight[j][0][k]) )

    # Position dictionary maps node id to [x, y] value
    pos = dict()
    for j in node_pos_info:
        pos[j] = [ float(df_cord['vecvalue'][(df_cord['name'] == 'coordX:vector') & (df_cord['node_ids'] == j)].item()[0]), 
                   float(df_cord['vecvalue'][(df_cord['name'] == 'coordY:vector') & (df_cord['node_ids'] == j)].item()[0]) ]

    # rcvd_edge_dict maps (edge from node, edge to node) pairs to a number of rcvd uni pks
    rcvd_edge_dict = dict()
    for j in range(len(rcvd_sdw)):
        rcvd_edge_dict[ (rcvd_sdw[j][0], rcvd_sdw[j][1]) ] = str(rcvd_sdw[j][2])

    # send_edge_dict maps (edge from node, edge to node) pairs to a number of rcvd uni pks
    send_edge_dict = dict()
    for j in range(len(send_sdw)):
        send_edge_dict[ (send_sdw[j][0], send_sdw[j][1]) ] = str(send_sdw[j][2])

    # color_edge_dict maps (edge from node, edge to node) pairs to a rgb triplet for weight color
    rcvd_color_edge_dict = dict()
    for j in range(len(rcvd_sdw)):
        rcvd_color_edge_dict[ (rcvd_sdw[j][0], rcvd_sdw[j][1]) ] = color_map_color(rcvd_sdw[j][2], cmap_name='viridis', vmin=min((tple[2] for tple in rcvd_sdw)), vmax=max((tple[2] for tple in rcvd_sdw)))

    # color_edge_dict maps (edge from node, edge to node) pairs to a rgb triplet for weight color
    send_color_edge_dict = dict()
    for j in range(len(send_sdw)):
        send_color_edge_dict[ (send_sdw[j][0], send_sdw[j][1]) ] = color_map_color(send_sdw[j][2], cmap_name='viridis', vmin=min((tple[2] for tple in send_sdw)), vmax=max((tple[2] for tple in send_sdw)))


    #xymax = int(grid[ (grid['run'] == i) & (grid['attrname'] == 'X') ]['attrvalue']) + int(50)
    xymax = 720
    # Recived plot
    fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(15,15))
    ax1.set(xlim=[0, xymax], ylim=[0, xymax])

    nodes_1 = netgraph.draw_nodes(node_positions=pos, 
                                  node_shape='o', 
                                  node_size=xymax*1.5, 
                                  node_edge_width=0.5,
                                  node_color='b',
                                  ax=None)

    nodes_label_1 = netgraph.draw_node_labels(node_labels={ii : ii for ii in pos},
                                              node_positions=pos,
                                              node_label_font_size=10,
                                              ax=None)

    edges_1 = netgraph.draw_edges(edge_list={(tple[0], tple[1]) for tple in rcvd_sdw},
                                  node_positions=pos,
                                  node_size=xymax*1.5,
                                  edge_width=xymax*1.5,
                                  edge_color=rcvd_color_edge_dict,
                                  draw_arrows=True,
                                  ax=None)

    edges_label_1 = netgraph.draw_edge_labels(edge_list={(tple[0], tple[1]) for tple in rcvd_sdw},
                                              edge_labels={key : value for key, value in rcvd_edge_dict.items()},
                                              node_positions=pos,
                                              edge_label_position=0.5,
                                              edge_label_font_size=10,
                                              edge_label_bbox=dict(alpha=0.),
                                              edge_width=xymax*1.5,
                                              ax=None)

    plt.xlabel("distance in meter", fontsize=18)
    plt.ylabel("distance in meter", fontsize=18)
    plt.title("Recived unicast packets", fontsize=24)
    cax1 = fig1.add_axes([0.905, 0.125, 0.02, 0.755])
    cmap1 = mpl.cm.viridis
    norm1 = mpl.colors.Normalize(vmin=min((tple[2] for tple in rcvd_sdw)), vmax=max((tple[2] for tple in rcvd_sdw)))
    cb1 = fig1.colorbar(mpl.cm.ScalarMappable(norm=norm1, cmap=cmap1), ax=ax1, cax=cax1, orientation='vertical')
    cb1.set_label('Number of recieved packets', fontsize=18)
    plt.show()



    # Send plot
    fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(15,15))
    ax2.set(xlim=[0, xymax], ylim=[0, xymax])

    nodes_2 = netgraph.draw_nodes(node_positions=pos, 
                                  node_shape='o', 
                                  node_size=xymax*1.5, 
                                  node_edge_width=0.5,
                                  node_color='b',
                                  ax=None)

    nodes_label_2 = netgraph.draw_node_labels(node_labels={ii : ii for ii in pos},
                                              node_positions=pos,
                                              node_label_font_size=10,
                                              ax=None)

    edges_2 = netgraph.draw_edges(edge_list={(tple[0], tple[1]) for tple in send_sdw},
                                  node_positions=pos,
                                  node_size=xymax*1.5,
                                  edge_width=xymax*1.5,
                                  edge_color=send_color_edge_dict,
                                  draw_arrows=True,
                                  ax=None)

    edges_label_2 = netgraph.draw_edge_labels(edge_list={(tple[0], tple[1]) for tple in send_sdw},
                                              edge_labels={key : value for key, value in send_edge_dict.items()},
                                              node_positions=pos,
                                              edge_label_position=0.5,
                                              edge_label_font_size=10,
                                              edge_label_bbox=dict(alpha=0.),
                                              edge_width=xymax*1.5,
                                              ax=None)

    plt.xlabel("distance in meter", fontsize=18)
    plt.ylabel("distance in meter", fontsize=18)
    plt.title("Send unicast packets", fontsize=24)
    cax2 = fig2.add_axes([0.905, 0.125, 0.02, 0.755])
    cmap2 = mpl.cm.viridis
    norm2 = mpl.colors.Normalize(vmin=min((tple[2] for tple in send_sdw)), vmax=max((tple[2] for tple in send_sdw)))
    cb2 = fig2.colorbar(mpl.cm.ScalarMappable(norm=norm2, cmap=cmap2), ax=ax2, cax=cax2, orientation='vertical')
    cb2.set_label('Number of transmitted packets', fontsize=18)
    plt.show()



    # Rolf plot
    node_weight = dict()
    for j in pos:
        if(j not in send_links_dst):
            node_weight[j] = 0
        else:
            idx = send_links_dst.index(j)
            node_weight[j] = sum(send_links_weight[idx][0])

    node_color_edge_dict = dict()
    for j in node_weight:
        node_color_edge_dict[j] = color_map_color(node_weight[j], cmap_name='autumn_r', vmin=min(node_weight.values()), vmax=max(node_weight.values()))

    plr_edge = dict()
    for key in send_edge_dict:
        plr_edge[key] = float(1) - (float(rcvd_edge_dict.get(key)) / float(send_edge_dict.get(key)))

    plr_color_edge = dict()
    for key in plr_edge:
        plr_color_edge[key] = color_map_color(plr_edge.get(key), cmap_name='winter_r', vmin=min(plr_edge.values()), vmax=max(plr_edge.values()))

    fig3, ax3 = plt.subplots(nrows=1, ncols=1, figsize=(15,15))
    ax3.set(xlim=[100, xymax], ylim=[100, xymax])

    nodes_3 = netgraph.draw_nodes(node_positions=pos, 
                                  node_shape='o', 
                                  node_size=xymax*3.5, 
                                  node_edge_width=0.5,
                                  node_color=node_color_edge_dict,
                                  ax=None)

    nodes_label_3 = netgraph.draw_node_labels(node_labels=node_weight,
                                              node_positions=pos,
                                              node_label_font_size=18,
                                              ax=None)

    edges_3 = netgraph.draw_edges(edge_list=plr_edge,
                                  node_positions=pos,
                                  node_size=xymax*3.5,
                                  edge_width=xymax*3.5,
                                  edge_color=plr_color_edge,
                                  draw_arrows=True,
                                  ax=None)

    edges_label_3 = netgraph.draw_edge_labels(edge_list={(tple[0], tple[1]) for tple in send_sdw},
                                              edge_labels={key : "%.3f" % round(value, 3) for key, value in plr_edge.items()},
                                              node_positions=pos,
                                              edge_label_position=0.5,
                                              edge_label_font_size=20,
                                              edge_label_bbox=dict(alpha=0.),
                                              edge_width=xymax*3.5,
                                              ax=None)

    plt.xlabel("distance in meter", fontsize=28)
    plt.ylabel("distance in meter", fontsize=28)
    plt.title("PLR between nodes and node activity", fontsize=34)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)

    cax31 = fig3.add_axes([0.905, 0.125, 0.02, 0.755])
    cmap31 = mpl.cm.autumn_r
    norm31 = mpl.colors.Normalize(vmin=min(node_weight.values()), vmax=max(node_weight.values()))
    cb31 = fig3.colorbar(mpl.cm.ScalarMappable(norm=norm31, cmap=cmap31), ax=ax3, cax=cax31, orientation='vertical')
    cb31.set_label('Number of transmitted packets', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)

    cax32 = fig3.add_axes([0.99, 0.125, 0.02, 0.755])
    cmap32 = mpl.cm.winter_r
    norm32 = mpl.colors.Normalize(vmin=min(plr_edge.values()), vmax=max(plr_edge.values()))
    cb32 = fig3.colorbar(mpl.cm.ScalarMappable(norm=norm32, cmap=cmap32), ax=ax3, cax=cax32, orientation='vertical')
    cb32.set_label('Link PLR', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.show()


# %%
df_fwd  = grid[ (grid['run'] == i) & ( (grid['type'] == 'scalar') &  (grid['name'] == 'ForwardedUnicasts:count') ) ].copy()
df_fwd1 = grid[ (grid['run'] == i) & ( (grid['type'] == 'vector') & (grid['module'] == 'Batmanadv.batman5Router[1].Batman5Routing') & (grid['name'] == 'ForwardedUnicasts:vector(packetBytes)') ) ].copy()
df_fwd5 = grid[ (grid['run'] == i) & ( (grid['type'] == 'vector') & (grid['module'] == 'Batmanadv.batman5Router[5].Batman5Routing') & (grid['name'] == 'ForwardedUnicasts:vector(packetBytes)') ) ].copy()

df_fwd1['vectime'] = df_fwd1['vectime'].apply(clean_alt_list)
df_fwd1['vectime'] = df_fwd1['vectime'].apply(eval)

df_fwd5['vectime'] = df_fwd5['vectime'].apply(clean_alt_list)
df_fwd5['vectime'] = df_fwd5['vectime'].apply(eval)

d1 = (df_fwd1['vectime']).tolist()
d1[0] = [float(i) for i in d1[0]]
d5 = (df_fwd5['vectime']).tolist()
d5[0] = [float(i) for i in d5[0]]

y1 = np.full((len(d1[0]),1), 1.1)
y5 = np.full((len(d5[0]),1), 1.2)
#%%
f = plt.figure(figsize=(8, 2))
plt.ylim(1.0, 1.3)
plt.plot(d1[0],y1,'o',color='orange', markersize=1)
plt.plot(d5[0],y5,'o',color='cornflowerblue', markersize=1)
plt.legend(['Yellow route', 'Blue route'], ncol=2, loc='lower center', bbox_to_anchor=(0.5, -0.50), fancybox=True, shadow=False)
plt.yticks([])
plt.xlabel('Time (s)')
plt.show()
f.savefig("tapt.pdf", bbox_inches='tight')

# %%
