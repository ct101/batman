#!/bin/bash

pushd ../../..; (make makefiles; make); [[ "$?" != 0 ]] && popd; popd

CONFIGS=( $(opp_run -a | gawk 'match($0, /Config /) {printf("%s\n"), substr($2, 1, length($2)-1)}') )
PS3='Enter # of config you wish to run: '
select i in ${CONFIGS[@]};
do
	SELECTED_CONFIG=$i
	break
done


# Maybe not the most optimal way of doing it... Needs to be tested
MAX_CORES=$1
NR_OF_RUNS=$(( $(opp_run -s -c $SELECTED_CONFIG omnetpp.ini -q runs | wc -l)-1 ))
for j in $(seq 0 $NR_OF_RUNS);
do
	opp_run -r $j -m -u Cmdenv -c $SELECTED_CONFIG --cmdenv-status-frequency=180s -n ../../../src:../..:../../../tutorials:../../../showcases --image-path=../../../images -l ../../../src/INET omnetpp.ini &

	background=( $(jobs -p) )
	if (( ${#background[@]} == $MAX_CORES ));
	then
		wait -n
	fi
done


# Apparently a better way for running things in parallel
#MAX_CORES=$1
#NR_OF_RUNS=$(( $(opp_run -s -c $SELECTED_CONFIG omnetpp.ini -q runs | wc -l)-1 ))
#seq 0 $NR_OF_RUNS | parallel -j $1 'opp_run -r {} -m -u Cmdenv -c "'$SELECTED_CONFIG'" --cmdenv-status-frequency=180s -n ../../../src:../..:../../../tutorials:../../../showcases --image-path=../../../images -l ../../../src/INET omnetpp.ini'
