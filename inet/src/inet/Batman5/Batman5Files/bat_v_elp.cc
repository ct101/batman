//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "../Batman5Files/bat_v_elp.h"

namespace inet {

bat_v_elp::bat_v_elp() {}
bat_v_elp::~bat_v_elp() {}

/**
 * createELPPacket: Creating an random ELP-packet
 */
batadv_elp_packet* Batman5Routing::createELPPacket(batadv_hard_iface *hard_iface){

    batadv_elp_packet *packet = new batadv_elp_packet();
    packet->setOrig(hostMAC);
    packet->setSeqno(hard_iface->bat_v.elp_seqno);
    packet->setElp_interval(hard_iface->bat_v.elp_interval);
    hard_iface->bat_v.elp_seqno += 1;

//    std::cout << hostModule->getFullName() << ": Creating ELP-Packet with Bytelength: " << packet->getByteLength() << std::endl;
//    std::cout << hostModule->getFullName() << " created ELP-Packet with seqno " << packet->getSeqno() << std::endl;
    return packet;

}//createELPPacket

batadv_elpProbe_packet* Batman5Routing::createELPProbePacket(batadv_hard_iface *hard_iface, uint8_t flag, int probeSize, MACAddress addr){
    batadv_elpProbe_packet *packet = new batadv_elpProbe_packet();
    packet->setOrig(hostMAC);
    packet->setSeqno(hard_iface->bat_v.elp_seqno);
    packet->setElp_interval(hard_iface->bat_v.elp_interval);
    hard_iface->bat_v.elp_seqno += 1;
    //packet->setByteLength(par("sizeELPProbes"));
    packet->setByteLength(probeSize);
    packet->setTimestamp();
    packet->setFirst_flag(flag);
    auto it = hard_iface->estimationDataContainer.find(addr);
    if(it != hard_iface->estimationDataContainer.end()){
        packet->setBitrate(it->second.last_known_bitrate);
        packet->setRcvd_packets(it->second.rcvd_packets); // Er det den rigtige tjek senere!!!
        packet->setRcvd_packets_window(lossWindowSize);
        packet->setCh_usage_ratio(hard_iface->own_usage_ratio);
        packet->setIndividual_usage_ratio(it->second.last_known_ch_usage_ratio);
//        cout << "individual usage: " << hostModule->getFullName() << it->second.last_known_ch_usage_ratio << endl;
        packet->getRts_usage_map().insert(hard_iface->rts_map.begin(),hard_iface->rts_map.end());
//        for(map<MACAddress,double>::iterator it_usage = packet->getRts_usage_map().begin(); it_usage != packet->getRts_usage_map().end(); ++it_usage) {
//            cout << "SEND: " << hostModule->getFullName() << " MAC: " << it_usage->first << " usage: " << it_usage->second << endl;
//        }
    }
    return packet;
}//createELPProbePacket

/**
 * batadv_v_elp_iface_enable() - setup the ELP interface private resources
 * @hard_iface: interface for which the data has to be prepared
 * Return: 0 on success
 */
int Batman5Routing::batadv_v_elp_iface_enable(batadv_hard_iface *hard_iface){
    if(hard_iface->if_num == 0){
        ELP_reminder_wlan0 = new elp_reminder();
        ELP_reminder_wlan0->hard_iface = hard_iface;
        ELP_reminder_wlan0->iface_id = hard_iface->if_id;
        scheduleAt(0+init_jitter,ELP_reminder_wlan0);

        // Timeout works for all Interfaces
        // initialized with first possible interface
        hardiface_neighborTableTimeout = new cMessage("neightable_timeout");
        scheduleAt(0+neigh_timeout_interval,hardiface_neighborTableTimeout);
    }
    else if(hard_iface->if_num == 1){
        ELP_reminder_wlan1 = new elp_reminder();
        ELP_reminder_wlan1->hard_iface = hard_iface;
        ELP_reminder_wlan1->iface_id = hard_iface->if_id;
        scheduleAt(0+init_jitter,ELP_reminder_wlan1);
    }
    else{
        return 1;
    }
    return 0;
}//batadv_v_elp_iface_enable


void Batman5Routing::batadv_v_elp_iface_disable(batadv_hard_iface *hard_iface){
    if(hard_iface->if_num == 0){
        ELP_reminder_wlan0->hard_iface = NULL;
        ELP_reminder_wlan0->iface_id = 0;
        cancelAndDelete(ELP_reminder_wlan0);
        cancelAndDelete(hardiface_neighborTableTimeout);
    }
    if(hard_iface->if_num == 1){
        ELP_reminder_wlan1->hard_iface = NULL;
        ELP_reminder_wlan1->iface_id = 0;
        cancelAndDelete(ELP_reminder_wlan1);
    }

}//batadv_v_elp_iface_disable

/**
 * batadv_v_elp_wifi_neigh_probe: send link probing packets to a neighbour
 *  returns true on success and false in case of error
 */
bool Batman5Routing::batadv_v_elp_wifi_neigh_probe(Hardif_neigh_node_ptr neigh){
    if(elpProbing){
        batadv_hard_iface *hard_iface = neigh->if_incoming;
        double last_tx_diff = 0;

        /* this probing routine is for Wifi neighbours only */
        if (!batadv_is_wifi_hardif(hard_iface)){
//            std::cout << "Interface is no wifi interface! No elpProbing!" << std::endl;
            return true;
        }

        /* probe the neighbor only if no unicast packets have been sent
         * to it in the last 100 milliseconds: this is the rate control
         * algorithm sampling interval (minstrel). In this way, if not
         * enough traffic has been sent to the neighbor, batman-adv can
         * generate 2 probe packets and push the RC algorithm to perform
         * the sampling
         */
/* Uncomment for non periodic
        last_tx_diff = (simTime()- neigh->bat_v->last_unicast_tx).dbl() * 1000 ;
//        std::cout << "Calculate last_tx_diff: " << last_tx_diff << std::endl;
        if (last_tx_diff <= BATADV_ELP_PROBE_MAX_TX_DIFF){
            return true;
        }
*/
//        std::cout << hostModule->getFullName() << " sending ELP-probes" << std::endl;
        if (numELPProbes == 3) {
            for (uint8_t i = 0; i < numELPProbes; i++) {
                if (i == 0 || i == 1) {
                    int sizeELPProbes = 16;
                    sendELPUnicast(neigh, i, sizeELPProbes);
                } 
                else {
                    int sizeELPProbes = par("sizeELPProbes");
                    sendELPUnicast(neigh, i, sizeELPProbes);
                }
            }
        }
        else {
            for (uint8_t i = 0; i < numELPProbes; i++) {
//                std::cout << "Probing Neighbor " << neigh->addr.str() << ": " << i << std::endl;
                sendELPUnicast(neigh, i, sizeELPProbes);
            }
        }
        return true;
    }
    else{
//        std::cout << "elpProbing is " << elpProbing << std::endl;
        return true;
    }
}//batadv_v_elp_wifi_neigh_probe


/**
 * batadv_v_elp_get_throughput: get the throughput towards a neighbour
 * Return: The throughput towards the given neighbour in multiples of 100kpbs
 *         (a value of '1' equals to 0.1Mbps, '10' equals 1Mbps, etc).
 */                                                                             // DELETE WHEN IT WORKS
//uint32_t Batman5Routing::batadv_v_elp_get_throughput(Hardif_neigh_node_ptr neigh){
//    batadv_hard_iface *hard_iface = neigh->if_incoming;
//    uint32_t throughput = 0;
//    int ret = 0;
//    int sizeELPProbes = par("sizeELPProbes");
//
//    /* if the user specified a customised value for this interface, then
//     * return it directly */
//    throughput = hard_iface->bat_v.throughput_override;
//    if (throughput != 0)
//        return throughput;
//
//    /* if this is a wireless device, then ask its throughput through
//    * cfg80211 API*/
//    if (batadv_is_wifi_hardif(hard_iface)) {
//        if (!batadv_is_cfg80211_hardif(hard_iface)){
////          std::cout << "unsupported WiFi driver version" << std::endl;
//            return BATADV_THROUGHPUT_DEFAULT_VALUE;
//        }
//        if(metricType.compare("throughputMetric") == 0){
//            auto it = hard_iface->estimationDataContainer.find(neigh->addr);
//            if(it != hard_iface->estimationDataContainer.end() && it->second.lastProbedThroughput != 0){
////              std::cout << hostModule->getFullName() << ": getting lastProbedThroughput = " << hard_iface->lastProbedThroughput << std::endl;
//                return it->second.lastProbedThroughput;
//            }
//        }
//        else if(metricType.compare("throughputWindowMetric") == 0){
//            auto it = hard_iface->estimationDataContainer.find(neigh->addr);
//            if(it != hard_iface->estimationDataContainer.end() && !it->second.throughput.empty()){
//                while(it->second.bitrate_time_stamp.front() < (simTime() - windowMaxAge) && !it->second.bitrate_time_stamp.empty()){
//                    it->second.bitrate_time_stamp.pop_front();
//                    it->second.bitrate_time_diff1.pop_front();
//                    it->second.bitrate_time_diff2.pop_front();
//
//                    if(it->second.bitrate_time_stamp.empty())
//                        break;
//                }
//                while(it->second.ch_usage_time_stamp.front() < (simTime() - ch_usage_window) && !it->second.ch_usage_time_stamp.empty()){
//                    it->second.ch_usage_time_stamp.pop_front();
//                    it->second.ch_usage_bits.pop_front();
//
//                    if(it->second.ch_usage_time_stamp.empty())
//                        break;
//                }
//                if(!it->second.bitrate_time_stamp.empty()){
//                    double avg_overhead = accumulate(it->second.bitrate_time_diff1.begin(), it->second.bitrate_time_diff1.end(), 0.0)/it->second.bitrate_time_diff1.size();
//                    //double avg_time_diff2 = accumulate(it->second.bitrate_time_diff2.begin(), it->second.bitrate_time_diff2.end(), 0.0)/it->second.bitrate_time_diff2.size();
//
//                    deque<double>::iterator it1 = min_element(it->second.bitrate_time_diff1.begin(), it->second.bitrate_time_diff1.end());
//                    deque<double>::iterator it2 = min_element(it->second.bitrate_time_diff2.begin(), it->second.bitrate_time_diff2.end());
//                    double avg_time_diff1 = *it1;
//                    double avg_time_diff2 = *it2;
//                    uint32_t bitrate = (((sizeELPProbes*8) - (16*8)) / (avg_time_diff2 - avg_time_diff1)) / 1000;
//                    it->second.last_known_bitrate = bitrate;
// 
//                    uint32_t total_bits = accumulate(it->second.ch_usage_bits.begin(), it->second.ch_usage_bits.end(), 0);
//                    double total_overhead = (double)it->second.ch_usage_bits.size() * avg_overhead;
//
//                    double total_usage = total_overhead + ((double)total_bits / (double)bitrate);
//                    double usage_ratio = total_usage / ch_usage_window.dbl();
//                    it->second.last_known_ch_usage_ratio = usage_ratio;
//
//                    return throughput;
//
//                    //uint32_t avarage = accumulate(it->second.throughput.begin(), it->second.throughput.end(), 0)/it->second.throughput.size();
//        //            cout << "Time: " << simTime() << " " << get_hardif_MAC(hard_iface->if_id) << " <-rec from-> " << neigh-> addr << " avg: " << avarage << endl;
//                    //return avarage;
//                }
//                else{
//                    return 0;
//                }
//            }
//            return 0;
//        }
//        cout << hostModule->getFullName() << "made it this far3!!!" << endl;
//        if (ret == -2 /*-ENOENT*/) {
//            /* Node is not associated anymore! It would be
//             * possible to delete this neighbor. For now set
//             * the throughput metric to 0. */
//            return 0;
//        }
//    }
//    else{
//        /* if not a wifi interface, check if this device provides data via
//         * ethtool (e.g. an Ethernet adapter) */
////          std::cout << "Ethtool-Throughput is set to Default" << std::endl;
//          // This simulation is written for VANETS and does not use ethernet
//          // So here the throughput is set to default !
//
//        if (!(hard_iface->bat_v.flags & BATADV_WARNING_DEFAULT)){
//            std::cout << "WiFi driver or ethtool info does not provide information about link speeds on interface " << hard_iface->name << " , therefore defaulting to hardcoded throughput values of " << BATADV_THROUGHPUT_DEFAULT_VALUE/10 <<"."<< BATADV_THROUGHPUT_DEFAULT_VALUE%10<< "Mbps." << std::endl;
//            hard_iface->bat_v.flags |= BATADV_WARNING_DEFAULT;
//        }
//
//        return BATADV_THROUGHPUT_DEFAULT_VALUE;
//    }
//}//batadv_v_elp_get_throughput


/**
 * batadv_v_elp_throughput_metric_update:
 * updating the throughput metric of a single hop neighbour
 */
void Batman5Routing::batadv_v_elp_throughput_metric_update(Hardif_neigh_node_ptr neigh_node, uint32_t new_metric){

    uint32_t throughput_metric = new_metric;

    ewma_metric_add(&neigh_node->bat_v->metric, throughput_metric);

    auto neigh_search = neigh_node->if_incoming->neigh_table->find(neigh_node->addr);
    if(neigh_search != neigh_node->if_incoming->neigh_table->end()){
        neigh_search->second->hardif_neigh->bat_v->metric = neigh_node->bat_v->metric;
    }
//    cout << "Host: " << hostModule->getFullName() << " Update node: " << neigh_node->addr << " metric: " << throughput_metric << endl;
}//batadv_v_elp_throughput_metric_update

/**
 * batadv_v_elp_periodic_work: periodic work of the Echo Location Protocol
 *              1. send broadcast ELP-packet
 *              2. probe known neighbors with unicast elp-packets
 *              3. update throughput
 */
void Batman5Routing::batadv_v_elp_periodic_work(batadv_hard_iface *hard_iface){
    // 1) Create and send ELP-packet
    sendELPBroadcast(hard_iface);

    hard_iface->own_usage_ratio = 0;
    for(map<MACAddress,throughput_data>::iterator it = hard_iface->estimationDataContainer.begin(); it != hard_iface->estimationDataContainer.end(); ++it) {
        hard_iface->own_usage_ratio += it->second.last_known_ch_usage_ratio;
    }

    hard_iface->rts_map.clear();
    for(int i = 0; i < hard_iface->rts_time_stamp.size(); i++) {
        if((simTime() - hard_iface->rts_time_stamp[i]) > 10) {
            hard_iface->rts_address.erase(hard_iface->rts_address.begin() + i);
            hard_iface->rts_usage.erase(hard_iface->rts_usage.begin() + i);
            hard_iface->rts_time_stamp.erase(hard_iface->rts_time_stamp.begin() + i);
        } else {
            hard_iface->rts_map.insert(std::pair<MACAddress,double>(hard_iface->rts_address[i], hard_iface->rts_usage[i]));
        }
    }

    /* Update Throuput metric on each sent packet:
     * 2) send a number of unicast ELPs for probing/sampling to each neighbor
     * 3) update the throughput metric value of each neighbor */
    for (Neighbor_Table::iterator ite = hard_iface->neigh_table->begin(); ite != hard_iface->neigh_table->end(); ite++){
        //std::cout << hostModule->getFullName() << ": Updating Throughput to known neighbors!" << std::endl;

        auto it = hard_iface->estimationDataContainer.find(ite->second->hardif_neigh->addr);
        if(it != hard_iface->estimationDataContainer.end()){

            while(it->second.bitrate_time_stamp.front() < (simTime() - windowMaxAge) && !it->second.bitrate_time_stamp.empty()){
                it->second.bitrate_time_stamp.pop_front();
                it->second.bitrate_time_diff1.pop_front();
                it->second.bitrate_time_diff2.pop_front();

                if(it->second.bitrate_time_stamp.empty())
                    break;
            }

            if(!it->second.bitrate_time_stamp.empty()){
                it->second.avg_overhead = accumulate(it->second.bitrate_time_diff1.begin(), it->second.bitrate_time_diff1.end(), 0.0)/it->second.bitrate_time_diff1.size();
                //double avg_time_diff2 = accumulate(it->second.bitrate_time_diff2.begin(), it->second.bitrate_time_diff2.end(), 0.0)/it->second.bitrate_time_diff2.size();

                deque<double>::iterator it1 = min_element(it->second.bitrate_time_diff1.begin(), it->second.bitrate_time_diff1.end());
                deque<double>::iterator it2 = min_element(it->second.bitrate_time_diff2.begin(), it->second.bitrate_time_diff2.end());
                double avg_time_diff1 = *it1;
                double avg_time_diff2 = *it2;
                uint32_t bitrate = (((sizeELPProbes*8) - (16*8)) / (avg_time_diff2 - avg_time_diff1)) / 1000;
                it->second.last_known_bitrate = bitrate;
            } else {
                //Edge case for lige i starten eller når de bevæger sig væk fra hinanden
                //Default værdierne er valgt til at undgå denne node som routing mulighed
                it->second.last_known_bitrate = 1;
                it->second.avg_overhead = 1.0;
            }

            while(it->second.ch_usage_time_stamp.front() < (simTime() - ch_usage_window) && !it->second.ch_usage_time_stamp.empty()){
                it->second.ch_usage_time_stamp.pop_front();
                it->second.ch_usage_bits.pop_front();

                if(it->second.ch_usage_time_stamp.empty())
                    break;
            }
            
            if(!it->second.ch_usage_time_stamp.empty()){
                uint32_t total_bits = accumulate(it->second.ch_usage_bits.begin(), it->second.ch_usage_bits.end(), 0);
                double total_overhead = (double)it->second.ch_usage_bits.size() * it->second.avg_overhead;

                double total_usage = total_overhead + ((double)total_bits / ((double)it->second.last_known_bitrate*1000));
                double usage_ratio = total_usage / ch_usage_window.dbl();
//                cout << hostModule->getFullName() << " avg: "<< it->second.avg_overhead << endl;
//                cout << hostModule->getFullName() << " total_bits: " << total_bits << " total_overhead: " << total_overhead << " total_usage: " << total_usage << " usage_ratio: " << usage_ratio << endl;
                if(usage_ratio > 1){
                    usage_ratio = 1;
                }
                it->second.last_known_ch_usage_ratio = usage_ratio;
            } else {
                it->second.last_known_ch_usage_ratio = 0;
            }

            if(it->second.recived_pkg_from_node.size() > lossWindowSize){
                it->second.recived_pkg_from_node.erase(it->second.recived_pkg_from_node.begin(),it->second.recived_pkg_from_node.end()-lossWindowSize);
            }
            if(it->second.recived_pkg_from_node_byte.size() > lossWindowSize){
                it->second.recived_pkg_from_node_byte.erase(it->second.recived_pkg_from_node_byte.begin(),it->second.recived_pkg_from_node_byte.end()-lossWindowSize);
            }

            it->second.rcvd_packets = accumulate(it->second.recived_pkg_from_node.begin(),it->second.recived_pkg_from_node.end(),0);

        }

        if(!batadv_v_elp_wifi_neigh_probe(ite->second->hardif_neigh)){
            /* if something goes wrong while probing, better to stop
             * sending packets immediately and reschedule the task */
            break;
        }
        //batadv_v_elp_throughput_metric_update(ite->second->hardif_neigh);
    }

}//batadv_v_elp_periodic_work

/**
 * handleELPPacket: main ELP packet handler
 * Handle an icoming ELP-packet and update the Neighbortable if necessary
 * returns NET_RX_DROP=1 if failed or NET_RX_SUCCESS=0 if succeded
 */
int Batman5Routing::handleELPPacket(batadv_elp_packet* packet, batadv_hard_iface *if_incoming, MACAddress src, MACAddress dest){
    MACAddress orig_address = packet->getOrig();
    bool res;
    int ret = NET_RX_DROP;

    res = batadv_check_management_packet(packet, if_incoming, BATADV_ELP_HLEN, src, dest);
    if (!res){
        std::cout << "ELP-Packet failed management packet Check! Deleting packet!" << std::endl;
        delete packet;
        return ret;
    }
    // am i the originator of the packet
    if (batadv_is_my_mac(orig_address)){
//        std::cout << "Received my own ELP-Packet! Deleting packet!" << std::endl;
        delete packet;
        return ret;
    }
    /* did we receive a B.A.T.M.A.N. V ELP packet on an interface
     * that does not have B.A.T.M.A.N. V ELP enabled ?
     * in this case not neccessary because only batman v nodes exist in this simulation */

//    std::cout << "Received ELP packet from " << orig_address << " seqno " << packet->getSeqno() << std::endl;

    batadv_v_elp_neigh_update(orig_address, if_incoming, packet);

    /* Getting Throughput from the received ELP Messages */
//    auto it = if_incoming->lastProbedThroughput.find(orig_address);
//    if(it != if_incoming->lastProbedThroughput.end()){
//       it->second = ( (packet->getBitLength()) / (simTime() - packet->getCreationTime()) ) / 1000;
//        std::cout << hostModule->getFullName() << ": Probed Throughput: " << it->second << "kbit/s, " << packet->getBitLength() << " <-> " <<  (simTime() - packet->getCreationTime()) << std::endl;
//    }

    ret = NET_RX_SUCCESS;
    emit(received_ELP, packet);
    delete packet;
    return ret;
}// HandleELPPacket


/*
 * ELP-Probes updates the variable lastProbedThroughput of an batadv_hard_iface
 * This can be read out when next time the "Driver" is asked for estimated throughput
 */
void Batman5Routing::handleELPProbePacket(batadv_elpProbe_packet* packet, batadv_hard_iface *if_incoming, MACAddress src, MACAddress dest){
//    std::cout << hostModule->getFullName() << ": Received ELP-Probe: " << packet->getByteLength() << " " << packet->getTimestamp() <<" " << simTime() << std::endl;
    
    auto it = if_incoming->estimationDataContainer.find(packet->getOrig());
    if(it != if_incoming->estimationDataContainer.end()){
        
        if((simTime()-it->second.loss_binTime_rcvd) < binSize && !it->second.recived_pkg_from_node.empty()){
            it->second.recived_pkg_from_node.back() = it->second.recived_pkg_from_node.back() + 1;
            it->second.recived_pkg_from_node_byte.back() = it->second.recived_pkg_from_node_byte.back() + packet->getByteLength();
        }else{
            it->second.recived_pkg_from_node.push_back(1);
            it->second.recived_pkg_from_node_byte.push_back(packet->getByteLength());
            it->second.loss_binTime_rcvd = simTime();
        }
        if(it->second.recived_pkg_from_node.size() > lossWindowSize){
            it->second.recived_pkg_from_node.erase(it->second.recived_pkg_from_node.begin(),it->second.recived_pkg_from_node.end()-lossWindowSize);
        }
        if(it->second.recived_pkg_from_node_byte.size() > lossWindowSize){
            it->second.recived_pkg_from_node_byte.erase(it->second.recived_pkg_from_node_byte.begin(),it->second.recived_pkg_from_node_byte.end()-lossWindowSize);
        }
    }

    emit(rcvd_ELP_probes, packet);

    if(numELPProbes == 3){
        if(metricType.compare("throughputWindowMetric") == 0){
            auto it = if_incoming->estimationDataContainer.find(packet->getOrig());
            if(it != if_incoming->estimationDataContainer.end()){
                if(packet->getFirst_flag() == 0){  
                    it->second.fistElpProbeTime = packet->getArrivalTime();
                    it->second.seqno = packet->getSeqno();
                    it->second.elp_flag = packet->getFirst_flag();
                    it->second.elp_flag2 = 0;
                }
                if(packet->getFirst_flag() == it->second.elp_flag + 1 && packet->getSeqno() == it->second.seqno + 1){
                    it->second.secondElpProbeTime = packet->getArrivalTime();
                    it->second.seqno = packet->getSeqno();
                    it->second.elp_flag = 0;
                    it->second.elp_flag2 = packet->getFirst_flag();
                }
                if(packet->getFirst_flag() == it->second.elp_flag2 + 1 && packet->getSeqno() == it->second.seqno + 1){
                    simtime_t time_between_1_and_2 = (it->second.secondElpProbeTime-it->second.fistElpProbeTime);
                    simtime_t time_between_2_and_3 = (packet->getArrivalTime()-it->second.secondElpProbeTime);
                    
                    it->second.bitrate_time_stamp.push_back(packet->getArrivalTime());
                    it->second.bitrate_time_diff1.push_back(time_between_1_and_2.dbl());
                    it->second.bitrate_time_diff2.push_back(time_between_2_and_3.dbl());

                    uint32_t bitrate = ( (packet->getBitLength() - (16*8)) / (time_between_2_and_3 - time_between_1_and_2) ) / 1000;

                    emit(sigbatTimeDiff, time_between_2_and_3);
                    emit(sigbatTimeDiff2, time_between_1_and_2);
                    

                    //RTS stuff
                    int rts_idx = -1;
                    for(int i = 0; i < if_incoming->rts_address.size(); i++) {
                        if(src == if_incoming->rts_address[i]) {
                            rts_idx = i;
                        }
                    }
                    if(rts_idx != -1){
                        if_incoming->rts_usage[rts_idx] = packet->getIndividual_usage_ratio();
                        if_incoming->rts_time_stamp[rts_idx] = simTime();
                    } else {
                        if_incoming->rts_address.push_back(src);
                        if_incoming->rts_usage.push_back(packet->getIndividual_usage_ratio());
                        if_incoming->rts_time_stamp.push_back(simTime());
                    }

                    //Read data from probe
                    it->second.rcvd_bitrate = packet->getBitrate();
                    it->second.rcvd_packets = packet->getRcvd_packets();
                    it->second.rcvd_window_size = packet->getRcvd_packets_window();
                    it->second.rcvd_usage = packet->getCh_usage_ratio();

                    //Read rts_map from probe
                    std::map<MACAddress, double> rts_recv;
                    rts_recv.insert(packet->getRts_usage_map().begin(), packet->getRts_usage_map().end());

                    //Clean rts_map (remove own mac + neighbours)
                    it->second.rts_neigh_usage = 0;
                    for(map<MACAddress,double>::iterator it_usage = rts_recv.begin(); it_usage != rts_recv.end(); ++it_usage) {
//                        cout << "BEFORE: " << hostModule->getFullName() << " MAC: " << it_usage->first << " usage: " << it_usage->second << endl;
                        if(it_usage->first != dest) {
                            int rts_flag = 0;
                            for (Neighbor_Table::iterator it_neigh = if_incoming->neigh_table->begin(); it_neigh != if_incoming->neigh_table->end(); it_neigh++){
                                if (it_usage->first == it_neigh->second->hardif_neigh->addr) {
                                    rts_flag = 1;
                                }
                            }
                            if(rts_flag != 1){
                                it->second.rts_neigh_usage += it_usage->second;
//                                cout << "AFTER: " << hostModule->getFullName() << " MAC: " << it_usage->first << " usage: " << it_usage->second << endl;
                            }
                        } 
                    }
                    //Sum values
                //    it->second.rts_neigh_usage = 0;
                //    for(map<MACAddress,double>::iterator it_usage = rts_recv.begin(); it_usage != rts_recv.end(); ++it_usage) {
                //        it->second.rts_neigh_usage += it_usage->second;
                //        cout << "AFTER: " << hostModule->getFullName() << " MAC: " << it_usage->first << " usage: " << it_usage->second << endl;
                //    }
                //    cout << endl;

                    //Calc and update loss/successrate
                    double pkg_send = accumulate(it->second.send_pkg_to_node.begin(),it->second.send_pkg_to_node.end(),0);
                    it->second.last_known_successrate = ((double)it->second.rcvd_packets / pkg_send);
                    //Incase successrate is > than 1, set it to 1
                    if(it->second.last_known_successrate > 1){
                        it->second.last_known_successrate = 1;
                    }

                    //Usage Calculation
                    if_incoming->total_usage_ratio = if_incoming->own_usage_ratio;
                    for(map<MACAddress,throughput_data>::iterator it_usage = if_incoming->estimationDataContainer.begin(); it_usage != if_incoming->estimationDataContainer.end(); ++it_usage) {
                        if_incoming->total_usage_ratio += it_usage->second.rcvd_usage;
                        if_incoming->total_usage_ratio += it_usage->second.rts_neigh_usage;
                    }
                    //Incase total usage is > than 1, set it to 1
                    if(if_incoming->total_usage_ratio > 1) {
                        if_incoming->total_usage_ratio = 1;
                    }
                    
                    //Calc new throughput
                    it->second.throughput = it->second.rcvd_bitrate;// * (1 - if_incoming->total_usage_ratio); //* it->second.last_known_successrate;// 
                    emit(sigbatBitrate, (long)it->second.rcvd_bitrate);
                    emit(sigbatUsage, (double)(1 - if_incoming->total_usage_ratio));
                    emit(sigbatSucessrate, it->second.last_known_successrate);
                    emit(sigbatThroughput, (double)it->second.throughput);
                    L3AddressResolver L3AddrRe;
                    emit(sigbatBitrateId, L3AddrRe.L3AddressResolver::findHostWithAddress(arp_table->getL3AddressFor(packet->getOrig()).toIPv4())->getIndex());
                    it->second.time_stamp = (packet->getArrivalTime());
//                    cout << "Host: " << hostModule->getFullName() << " Estimeret bitrate    : " << it->second.rcvd_bitrate << endl;
//                    cout << "Host: " << hostModule->getFullName() << " Estimeret usage ratio: " << if_incoming->total_usage_ratio << endl;
//                    cout << "Host: " << hostModule->getFullName() << " Estimeret successrate: " << it->second.last_known_successrate << endl;

                    //Fresh new throughput
                    auto it_neigh = if_incoming->neigh_table->find(packet->getOrig());
//                    cout << "Host: " << hostModule->getFullName() << " Original src: " << src << endl;
                    batadv_v_elp_throughput_metric_update(it_neigh->second->hardif_neigh, it->second.throughput);
                }
            }
        }
    }
    else {
        // If number of elp probes != 3
        if(metricType.compare("throughputWindowMetric") == 0){
            auto it = if_incoming->estimationDataContainer.find(packet->getOrig());
            if(it != if_incoming->estimationDataContainer.end()){
                if(!packet->getFirst_flag()){  
                    it->second.fistElpProbeTime = packet->getArrivalTime();
        //                cout << "First packet; first: " << probemac->first << " second " << probemac->second << " orig " << " Sqeno " << packet->getSeqno() << endl;
                }
                if(packet->getFirst_flag()){
                    //cout << "Second packet; first: " << probemac->first << " second " << probemac->second << " Seqno " << packet->getSeqno() << endl;          
        //            cout << "first arrival: " << probemac->second << " second arrival: " << packet->getArrivalTime() << " length: " << packet->getByteLength() << endl;
                    uint32_t bitrate = ((packet->getBitLength()) / (packet->getArrivalTime()-it->second.fistElpProbeTime)) / 1000;
    //////                it->second.throughput.push_back(bitrate);
    //////                it->second.time_stamp.push_back(packet->getArrivalTime());
                }
                //it->second = ( (packet->getBitLength()) / (simTime() - packet->getTimestamp()) ) / 1000;
        //        std::cout << hostModule->getFullName() << ": Probed Throughput: " << it->second << "kbit/s" << std::endl;
            }
        }
    }
    delete packet;
}

/**
 * batadv_v_elp_neigh_update:
 * Updates the ELP neighbour node state with the data received within the new ELP packet.
 */
void Batman5Routing::batadv_v_elp_neigh_update(MACAddress neigh_addr, batadv_hard_iface *if_incoming, batadv_elp_packet* elp_packet){
    Neigh_node_ptr neigh = NULL;
    Orig_node_ptr orig_neigh = NULL;
    Hardif_neigh_node_ptr hardif_neigh = NULL;
    int32_t seqno_diff;
    int32_t elp_latest_seqno;

    orig_neigh = batadv_v_ogm_orig_get(neigh_addr);
    if (orig_neigh == NULL){
        // NULL only in case of failure
        return;
    }
    neigh = batadv_neigh_node_get_or_create(orig_neigh, if_incoming, neigh_addr);
    if (neigh == NULL){
            return;
    }

    hardif_neigh = batadv_hardif_neigh_get(if_incoming, neigh_addr);
    if (hardif_neigh == NULL){
            return;
    }

    elp_latest_seqno = hardif_neigh->bat_v->elp_latest_seqno;
    seqno_diff = elp_packet->getSeqno()- elp_latest_seqno;

    /* known or older sequence numbers are ignored. However always adopt
     * if the router seems to have been restarted. */
    if (seqno_diff < 1 && seqno_diff > -BATADV_ELP_MAX_AGE){
        return;
    }

    /* Passed the tests -> update elp neigh */
//    std::cout << hostModule->getFullName() << ": Update ELP Neighbor -> " << neigh_addr << ", " << simTime() << std::endl;
    auto search_neigh = orig_neigh->neigh_list->find(neigh_addr);
    search_neigh->second->last_seen = simTime();
    search_neigh->second->hardif_neigh->last_seen = simTime();
    search_neigh->second->hardif_neigh->bat_v->elp_latest_seqno = elp_packet->getSeqno();

    search_neigh->second->hardif_neigh->bat_v->elp_interval = (double)elp_packet->getElp_interval() / 1000; // uint32 in ms to simtime

//    std::cout << "uint32_t in ms to simtime: " << elp_packet->getElp_interval() << " -> " << (double)elp_packet->getElp_interval()/1000 << ", current: " << search_neigh->second->hardif_neigh->bat_v->elp_interval << endl;

    // if neighbor node is not known yet
    auto probeSearch = if_incoming->estimationDataContainer.find(neigh_addr);
    if (probeSearch == if_incoming->estimationDataContainer.end()){
        // Add new lastProbedThroughput-entry for neighbor
        throughput_data initThroughputData;
        if_incoming->estimationDataContainer.insert(std::pair<MACAddress,throughput_data>(neigh_addr, initThroughputData));
       // if_incoming->fistElpProbeTime.insert(std::pair<MACAddress,simtime_t>(neigh_addr, 0));
    }

}//batadv_v_elp_neigh_update

} /* namespace inet */
