/*
 * metric.cpp
 *
 */

#include "Batman5Metric.h"

#include "../../Batman5/routing/Batman5Routing.h"

namespace inet {

void Batman5Routing::initializeMetric(){

    /**
     * Check if metricType is a known type
     * if not set it to default hopPenalty
     */

    this->metricType = par("metricType").stdstringValue();

    if(metricType.compare("throughput_overwrite") == 0){
        return;
    }
    else if(metricType.compare("throughputMetric") == 0){
        return;
    }
    else if(metricType.compare("throughputWindowMetric") == 0){
        return;
    }
    else{
        std::cout << "metricType " << metricType << " is not known! Using default metricType!" << std::endl;
        this->metricType = "hopPenalty";
        return;
    }

}//initializeMetric

void Batman5Routing::ewma_metric_init(ewma_Metric *avg){
    avg->factor = log2(10);
    avg->weight = log2(8);
    avg->internal = 0; // this is the metric value
}

/*
 * Determine Linkmetric
 */
uint32_t Batman5Routing::getLinkMetric(Hardif_neigh_node_ptr neigh){        // DELETE WHEN IT WORKS

    /**
     * Here the link metric to a neighbor node is determined
     * this function will be called in order of the ELP
     * batadv_v_elp_throughput_metric_update leads to this method
     *
     * Call the calculation matching the metricType
     */
    if(metricType.compare("throughput_overwrite") == 0){
        return 1;//batadv_v_elp_get_throughput(neigh);
    }
    else if(metricType.compare("throughputMetric") == 0){
        return 1;//batadv_v_elp_get_throughput(neigh);
    }
    else if(metricType.compare("throughputWindowMetric") == 0) {
        return 1;//batadv_v_elp_get_throughput(neigh);
    }
    else{
        return 0;
    }
}

/*
 * Determine Pathmetric
 */
void Batman5Routing::metricMatchLinkCharacteristicts(Hardif_neigh_node_ptr hardif_neigh, batadv_ogm2_packet *ogm_packet, MACAddress neigh){

    /**
     * Here the ogm_metric can be configured to match link characteristics
     * e.g. using path_metric or link_metric
     */

    /* Update the received throughput metric to match the link characteristic:
     *  - If this OGM traveled one hop so far (emitted by single hop neighbor)
     *    the path throughput metric equals the link throughput.
     *  - For OGMs traversing more than one hop the path throughput metric is
     *    the smaller of the path throughput and the link throughput. */

    uint32_t link_metric = getEWMAMetric(&hardif_neigh->bat_v->metric);
    uint32_t path_metric = std::min(link_metric, ogm_packet->getMetric());
    emit(sigbatWindowBitrate, (long)link_metric);
    L3AddressResolver L3AddrRe;
    emit(sigbatWindowBitrateId, L3AddrRe.L3AddressResolver::findHostWithAddress(arp_table->getL3AddressFor(neigh).toIPv4())->getIndex());
    ogm_packet->setMetric(path_metric);
}

/*
 * Determine forwarding Pathmetric
 */
uint32_t Batman5Routing::calculatePathMetric(batadv_hard_iface *if_incoming, batadv_hard_iface *if_outgoing, uint32_t metric, uint8_t ttl){

    /**
     *  Here the path_metric is calculated with additional information
     *  and the Metric of the last OGMv2.
     *
     *  Call the calculation matching the metricType
     */

    if(metricType.compare("throughput_overwrite") == 0){
        return batadv_v_forward_penalty(if_incoming, if_outgoing, metric, ttl);
    }
    else if(metricType.compare("throughputMetric") == 0){
        return batadv_v_forward_penalty(if_incoming, if_outgoing, metric, ttl);
    }
    else if(metricType.compare("throughputWindowMetric") == 0){
        return batadv_v_forward_penalty(if_incoming, if_outgoing, metric, ttl);
    }
    else{
        return 0;

    }
}

uint32_t Batman5Routing::getEWMAMetric(ewma_Metric *avg){
    if(metricType.compare("throughputMetric") == 0){
        return ewma_metric_read(avg);
    }
    else{
        return avg->internal;
    }
}

uint32_t Batman5Routing::ewma_metric_read(ewma_Metric *avg){
    if(metricType.compare("throughputMetric") == 0){
        return avg->internal >> avg->factor;
    }
}

/**
 * Using the same implementation of EWMA as in batman v
 */
ewma_Metric *Batman5Routing::ewma_metric_add(ewma_Metric *avg, uint32_t value){

//    std::cout << hostModule->getFullName() << ": Adding new value: " << value << " to ewma_Metric: " << getEWMAMetric(avg) << std::endl;
    if(metricType.compare("throughputMetric") == 0){
        avg->internal = avg->internal  ? (((avg->internal << avg->weight) - avg->internal) + (value << avg->factor)) >> avg->weight : (value << avg->factor);
    }
    else{
        avg->internal = value;
    }
    //    std::cout << " -> " << ewma_throughput_read(avg) << std::endl;
    return avg;
};


metric::metric() {}
metric::~metric() {}

} /* namespace inet */
